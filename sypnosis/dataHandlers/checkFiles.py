import json
import os

# list of filenames
files = []
# list of missing files
missing = []

# we will need this later in the program
jsonData = []

# lets get the list of files
# this part maybe have to be rewritten, if your json data isn't the same
with open('newLines.json') as lines:
    data = json.load(lines)
    for f in data:
        files.append(f['filename'])
        jsonData.append(
            {"filename": f['filename'],
             "dialog": f['dialog'],
             "length": len(f['dialog'])
             })

obj = json.load(open('lines.json'))

# check that it worked
# for f in files:
#    print(f)

# file end for every file
ending = ".wav.ogg.wav"

# path
# E:\ML sypnosis\audio\______sounds\wav\g_lines
path = "E:/ML sypnosis/audio/______sounds/wav/g_lines/"

# just to see if it works (should always show 1)
# files.append("test")

# checking if the filename have a file connected to them
for f in files:
    if not os.path.exists(path + f + ending):
        # # uncomment the print line when testing
        # print(path + f + ending)
        missing.append(f)

print("there are: ", len(missing), " missing files")

# write the missing files to a txt file
with open('missing.txt', 'w') as outfile:
    json.dump(missing, outfile)

# count before
print("count of json array: ", len(obj))

items = []
# get all the elements as json objs, so we can remove them after
for i in range(len(obj)):
    if obj[i]["filename"] in missing:
        items.append(obj[i])

for i in items:
    obj.remove(i)

print("count of json array: ", len(obj))

# sort it by the len
print("typeof:", obj)


# make a new json file, without the missing files
with open('newLinesTest.json', 'w') as outfile:
    json.dump(jsonData, outfile)