import json
import os
import soundfile as sf

# this will only be used for the root folder, were all the other folders starts

datasetLoc = "E:/ML sypnosis/other datasets/UrbanSound8K/audio"
# change value here ------------------------------------------------------------------------------------

# top folders that have sub folders in them
headFolders = {}

os.chdir(datasetLoc)

misses = []

# --------------------- get the list of all the folders --------------------------
counter = 1
# here we are going into our folderPath, then we are adding all top folders to "headFolders"
test = [name for name in os.listdir(".") if os.path.isdir(name)]
for value in test:
    headFolders[counter] = value
    counter += 1

# --------------------- read the each sound file and txt file pair  --------------------------

items = []
counter = 1
# here we are finding all sub folders and add them to "folders"
for key in headFolders:
    # jump into the sub folder
    tempPath = datasetLoc + "/" + headFolders[key]
    os.chdir(tempPath)

    # get a list of all sub folders in this folder

    files = [name for name in os.listdir(".") if os.path.isfile(name)]

    for ff in files:
        print(ff)

        soundFile = tempPath + "/" + ff
        q = sf.SoundFile(soundFile)

        items.append({
            "filename": ff,
            "path": tempPath + "/" + ff,
            "folder": headFolders[key],
            "samples": len(q),
            "sample_rate": q.samplerate,
            "seconds": (len(q) / q.samplerate)
        })

    print(headFolders[key] + " is done")

for i in items:
    print(i)

# print("count of missing speakers", len(speakers))
#
# lets export the data as a json file
script_dir = os.path.dirname(__file__) # the location of my script
os.chdir(script_dir)

newItems = sorted(items, key=lambda x: x["seconds"])

dataFile = open('urbanSound8k.json', 'w')
dataFile.write(json.dumps(newItems, indent=2))
#dataFile.write(wordData, indent=4, sort_keys=True)
dataFile.close()