import json
import os

# list of filenames
files = []
# list of missing files
missing = []

with open('train-clean-360.json') as lines:
    data = json.load(lines)
    for f in data:
        files.append(f)


#obj = json.load(open('dev-other.json'))


for f in files:
    if not os.path.exists(f["path"]):
        missing.append(f)

print("there are: ", len(missing), " missing files")

# write the missing files to a txt file
with open('missingFiles.txt', 'w') as outfile:
    json.dump(missing, outfile)

