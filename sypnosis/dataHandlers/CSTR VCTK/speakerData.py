import json
import glob, os

# ID  AGE  GENDER  ACCENTS  REGION
input = open('speaker-info.txt', 'r')

items = []
# go into the subfolder and find the *.txt file. then open it
for i in input:
    tempCopy = i
    test = i.split(" ")
    id = test[0]
    age =test[2]
    gender =test[3]
    accents = test[4]
    region = ""

    number = len(id + "  " + age + " " + gender + " " + accents + " ")
    if not i[number:] == "":
        # remove "\n" and made the "---" space to "-" space
        test = (str(i[number:]).replace("\n", "")).replace("  ", " ")
        region = test

    items.append({
        "id": id,
        "age": age,
        "gender": gender,
        "accents": accents.replace("\n", ""),
        "region": region
    })
    #print("--------------------")

for i in items:
    print(i)
dataFile = open('speakers.json', 'w')
dataFile.write(json.dumps(items, indent=2))
dataFile.close()