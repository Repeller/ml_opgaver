import json
import os

a = "E:/ML sypnosis/other datasets/CSTR VCTK/VCTK-Corpus/wav48"

speakers = []

with open('speakers.json') as persons:
    data = json.load(persons)
    for f in data:
        # change value here -----------------------------------------------------------------------------
        speakers.append(f)

# this will only be used for the root folder, were all the other folders starts
datasetLoc = "E:/ML sypnosis/other datasets/CSTR VCTK/VCTK-Corpus/wav48"
# change value here ------------------------------------------------------------------------------------


#

# top folders that have sub folders in them
headFolders = {}

os.chdir(datasetLoc)

missingSpeakers = []
misses = []

# --------------------- get the list of all the folders --------------------------
counter = 1
# here we are going into our folderPath, then we are adding all top folders to "headFolders"
test = [name for name in os.listdir(".") if os.path.isdir(name)]
for value in test:
    headFolders[counter] = value
    counter += 1

# --------------------- check folders with the speakers --------------------------

justFolders = []

for i in headFolders:
    justFolders.append(headFolders[i][1:])

for i in justFolders:
    if i not in speakers:
        misses.append("folder: " + i + " don't have a speaker")


# --------------------- read the each sound file and txt file pair  --------------------------
items = []
counter = 1
# here we are finding all sub folders and add them to "folders"
for key in headFolders:
    # jump into the sub folder
    tempPath = datasetLoc + "/" + headFolders[key]
    os.chdir(tempPath)

    # get a list of all sub folders in this folder
    #childFolders = len([name for name in os.listdir(".")])
    fileCount = len([name for name in os.listdir('.') if os.path.isfile(name)])

    person = None
    for s in speakers:
        g = str(headFolders[key][1:])
        if s["id"] == g:
            person = s
            break

    if person is None:
        person = {
            "id": headFolders[key],
            "age": "",
            "gender": "",
            "accents": "",
            "region": ""
        }
        missingSpeakers.append(person)

    for i in range(fileCount + 1):
        x = i + 1
        if len(str(x)) == 1:
            x = "00" + str(x)
        if len(str(x)) == 2:
            x = "0" + str(x)
        if len(str(x)) >= 3:
            x = str(x)

        if os.path.isfile((headFolders[key] + "_" + x + ".wav")) and os.path.isfile((headFolders[key] + "_" + x + ".txt")):
            soundFile = (headFolders[key] + "_" + x + ".wav")
            txtFile = open(headFolders[key] + "_" + x + ".txt", "r")
            lines = txtFile.readline()
            items.append({
                "filename": soundFile,
                "path": tempPath + "/" + soundFile,
                "dialog": lines,
                "len": len(lines),
                "gender": person["gender"],
                "age": person["age"],
                "accents": person["accents"],
                "region": person["region"]
            })
            txtFile.close()

    print(headFolders[key] + " is done")

for i in items:
    print(i)

print("count of missing speakers", len(speakers))

# lets export the data as a json file
script_dir = os.path.dirname(__file__) # the location of my script
os.chdir(script_dir)

newItems = sorted(items, key=lambda x: x["len"])

dataFile = open('cstr.json', 'w')
dataFile.write(json.dumps(newItems, indent=2))
#dataFile.write(wordData, indent=4, sort_keys=True)
dataFile.close()

dataFile = open('misses.txt', 'w')
dataFile.write(json.dumps(misses, indent=2))
#dataFile.write(wordData, indent=4, sort_keys=True)
dataFile.close()
