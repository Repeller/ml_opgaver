import json
import os

# this script is design to be run ones for each rar folder

a = "openslr/dev-clean/LibriSpeech/dev-clean"
b = "openslr/dev-other/LibriSpeech/dev-other"
c = "openslr/train-clean-100/LibriSpeech/train-clean-100"
d = "openslr/train-clean-360/LibriSpeech/train-clean-360"
e = "openslr/train-other-500/LibriSpeech/train-other-500"
ff = "openslr/test-clean/LibriSpeech/test-clean"

subsets = [
    {"a": "dev-clean"},
    {"b": "dev-other"},
    {"c": "train-clean-100"},
    {"d": "train-clean-360"},
    {"e": "train-other-500"},
    {"f": "test-clean"},
]

speakers = []

with open('speakers.json') as persons:
    data = json.load(persons)
    for f in data:
        # change value here -----------------------------------------------------------------------------
        if f["subset"] == "test-clean":
            speakers.append(f)

# this will only be used for the root folder, were all the other folders starts
datasetLoc = "E:/ML sypnosis/other datasets/"
folderPath = datasetLoc + ff
# change value here ------------------------------------------------------------------------------------


#

# top folders that have sub folders in them
headFolders = {}
folders = {} # hold values for both "headFolders" and "subFolders"

os.chdir(folderPath)

counter = 1
# here we are going into our folderPath, then we are adding all top folders to "headFolders"
test = [name for name in os.listdir(".") if os.path.isdir(name)]
for value in test:
    headFolders[counter] = value
    counter += 1


testing = ""
firstCheck = True

counter = 1
# here we are finding all sub folders and add them to "folders"
for key in headFolders:
    # jump into the sub folder
    tempPath = folderPath + "/" + headFolders[key]
    os.chdir(tempPath)
    # get a list of all sub folders in this folder
    childFolders = [name for name in os.listdir(".") if os.path.isdir(name)]
    for y in childFolders:
        # add the sub folder with both folder's names
        folders[counter] = {"topFolder": headFolders[key], "subFolder": y }
        counter += 1


items = []
# go into the subfolder and find the *.txt file. then open it
for i in folders:
    # jump into the sub folder, where there should be a txt file
    tempPath = folderPath + "/" + folders[i]["topFolder"] + "/" + folders[i]["subFolder"]
    os.chdir(tempPath)

    stringStart = folders[i]["topFolder"] + "-" + folders[i]["subFolder"] + "-"

    # get the speaker from this "headFolder"
    person = None
    for s in speakers:
        s_id = s["id"]
        if s_id == folders[i]["topFolder"]:
            person = s

    # look at all the files in this subfolder
    for file in os.listdir():
        if file.endswith(".txt"):
            txt = open(file, 'r')
            for q in txt:

                element = q.replace(stringStart, '')
                fullFilePath = tempPath + "/" + stringStart + element[:4] + ".flac"
                items.append({
                    "filename": stringStart + element[:4],
                    "path": fullFilePath,
                    "dialog": element[5:],
                    "len": len(element[5:]),
                    "sex": person["sex"],
                    "name": person["name"],
                    "subset": "test-clean"
                    # change value here ---------------------------------------------------------------------------------------------------------------------
                })
            txt.close()
# we are now inside the subfolder

# for i in items:
#    print(i)

# lets export the data as a json file
script_dir = os.path.dirname(__file__) # the location of my script
os.chdir(script_dir)

newItems = sorted(items, key=lambda x: x["len"])

# change value here ---------------------------------------------------------------------------------------------------------------------------------------------
dataFile = open('test-clean.json', 'w')
dataFile.write(json.dumps(newItems, indent=2, sort_keys=True))
#dataFile.write(wordData, indent=4, sort_keys=True)
dataFile.close()