import json
import glob, os

input = open('speakers.txt', 'r')

items = []
# go into the subfolder and find the *.txt file. then open it
for i in input:
    id = i[:5].strip()
    sex = i[6:8].strip()
    subset = (i[10:27]).strip()
    mins = i[30:33].strip()
    name = i[38:].strip()

    items.append({
        "id": id,
        "sex": sex,
        "subset": subset,
        "mins": mins,
        "name": name
    })

dataFile = open('speakers.json', 'w')
dataFile.write(json.dumps(items, indent=2))
dataFile.close()