import json
from collections import Counter

# jsonData = []

# all the unique words/lines
words = []
repeats = 0

# with open('newLinesTest.json') as lines:
#     data = json.load(lines)
#     for f in data:
#         jsonData.append(f)

# if the word is not in the "word"/line list, add it
# here we count the full number of repeats
with open('newLinesTest.json') as lines:
    data = json.load(lines)
    for f in data:
        if not f['dialog'] in words:
            words.append(f['dialog'])
        else:
            repeats += 1


noDoubleWords = list(dict.fromkeys(words))

print("repeats count: ", repeats)

print("words", type(words))
#print("jsonData", type(jsonData))

c = Counter(words)

wordData = []

# here we count how many times the word/line have been repeated
for i in noDoubleWords:
    y = words.count(i)
    if y > 1:
        wordData.append({"word": i, "count": y})

# make a file of dialog, that are doubles or more of in the dataset
dataFile = open('repeatsInDataset.json', 'w')
dataFile.write(json.dumps(wordData, indent=4))
#dataFile.write(wordData, indent=4, sort_keys=True)
dataFile.close()

# for i in words:
#   print("count: ", i['count'], " dialog: ", i['dialog'])
