# -*- coding: utf-8 -*-
"""
Created on Thu Feb  6 13:59:34 2020

@author: Ziegler
"""

#opgave 1
print("opg 1")

color = ["red", "big", "tasty"]
fruits = ["apple", "banana", "cherry"]

for c in color:
    for f in fruits:
        print(c, f)

#opgave 2
print("opg 2")

T = [[11,12,5,2], [15,6,10], [10,8,12,5], [12,15,8,6]]

print(T[1])
print(T[1][2])

T[1][2] = 99
# nr 2 list, nr 3 element will be sat to 99

print(T[1])
print(T[1][2])

for itemList in T:
    for item in itemList:
        print(item, end = " ")
    print()
    
#opgave 3
print("opg 3")

import random
data = [] # create an empty list
while len(data) < 10:
    randNo = random.randint(0,100)
    data.append(randNo)
print(data)

#opgave 4
print("opg 4")

def rand_func():
    tempData = []
    while len(tempData):
        randNo2 = random.randint(0,100)
        tempData.append(randNo2)
    
    print('rand-func',data)
    return tempData


output9 = rand_func()
print(output9)