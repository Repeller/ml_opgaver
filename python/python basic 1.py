# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""
import math

print('ziegler')

# opgave 1
print()

x = 1 
y = 3
z = 5 
sum = x+y+z
print(sum)

print("Sum is: " + str(sum))
print("Sum is: " + str(sum) + "DKK")
# finally use the f-strings, a new geature in python3
print(f"Sum is: + {sum} + DKK")

# opgave 2
print()

for i in range(1, 11):
    print(i, i*i, end = " ")
    print()
    
# opgave 3
print()
    
for i in range(1, 11):
    for j in range(1,11):
        print(i*j, end = " ")
    print()
    
# opgave 4
print()

i = 1
while (i <=11):
    print(i*i, end = " ")
    i += 1
    
# opgave 5
print()

x = 7 
y = 23
if (x > y):
    print("x: " + str(x))
else:
    print("y: " + str(x))
    
# opgave 6
print()

def findMax(x,y):
    if(x > y):
        return x
    else:
        return y

print("5 or 69: ", findMax(5, 69))

# opgave 7
print()

data = [1,2,3,5,7,11,13,17,19,23]
print(data)
for item in data:
    print(item, end = " ")

print()
# for loop go thouth all the index
    
print("nr 4" + str(data[3]))
data[0] = 17
data.append(37)
print(data)

# opgave 8
print()

def find_max(data):
    my_max_ = max(data)
    return my_max_

max_num = find_max(data)
print(max_num)

# opgave 9
print()

def find_min(data):
    my_min = min(data)
    return my_min

my_min = find_min(data)
print("min: ", my_min)

# opgave 10
print()

def _sum(data):
    my_sum = 0
    for item in data:
        my_sum += item
    return my_sum

mySum = _sum(data)
print("sum ", str(mySum))


# opgave 11
print()

def _average(data):
    total = _sum(data)
    return (total / len(data))

average = _average(data)
print("_average", str(average))

# opgave x
print("x")

print(data)

def highest_5_numbers(data):
    highest = []
    tempData = data
    i = 0
    while(i < 5):
        highNum = 0
        for item in tempData:
            if(item > highNum):
                highNum = item
            #end of fun
        tempData.remove(highNum)
        highest.append(highNum)
        i += 1
        
    return highest

num5 = highest_5_numbers(data)
print(num5)
    